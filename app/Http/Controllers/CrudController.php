<?php

namespace App\Http\Controllers;

use Auth;
use Alert;
use App\Project;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;

class CrudController extends Controller
{
    public function index() {
        if(Auth::user()->role == 'user') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
        $project = Project::all();
        $no = 1;
        return view('project.index', compact('project', 'no'));
    }

    public function create() {
        $no = 1;
        $response = Http::get('https://api.kawalcorona.com/indonesia/provinsi/');
        $data = $response->json();

        // dd($data);
        return view('project.kopit', compact('data','no'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'kategori' => 'required',
        ]);

       $image = $request->file('foto');
       $user = auth()->user();
       $extension  = $image->extension();

       $lokasi = '/uploads/images/project/'.$user->name ."_". time() . '.' . $extension; 
       $request->file('foto')->move("uploads/images/project", $lokasi);
       $foto = $lokasi;
 
        Project::create([
            'foto' => $foto,
            'nama' => $request->nama,
            'kategori' => $request->kategori,
        ]);
        
        alert()->success('Berhasil.','Data Project ditambahkan!');
        return redirect('project');

    }

    public function show($id)
    {
        $project = Project::find($id);
        if(Auth::user()->role == 'user') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
        return view('project.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function edit($id)
     {
         $project = Project::find($id);
        if(Auth::user()->role == 'user') {
            Alert::info('Oopss..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
       return view('project.edit', compact('project'));
     }
     
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'kategori' => 'required',
        ]);
       
        $project = Project::find($id);
       $image = $request->file('foto');
       $user = auth()->user();

       if (empty ($image = $request->file('foto'))) {
    
        Project::where('id',$id)->update([
            'foto' => $request->foto1,
            'nama' => $request->nama,
            'kategori' => $request->kategori,
            ]);
       } else {
        $extension  = $image->extension();

        if($project->foto != '/uploads/images/project/default.png') {
            File::delete(public_path().$project->foto);
    
            $lokasi = '/uploads/images/project/'.$user->name ."_". time() . '.' . $extension; 
            $request->file('foto')->move("uploads/images/project", $lokasi);
            $foto = $lokasi;
        } else {
            $foto = $request->foto;
        }
        
            Project::where('id',$id)->update([
                'foto' => $foto,
                'nama' => $request->nama,
                'kategori' => $request->kategori,
                ]);
       }
       
     
        alert()->success('Berhasil','Data Project Telah diubah!');
        return redirect('project');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function destroy($id)
    {
        $item = Project::findOrFail($id);
        $item->delete();
        return redirect()->route('project');
    }
}
