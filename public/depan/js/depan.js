$(document).ready(function() {
    $('a[href^="#home"]').addClass("active");

    //smoothscroll
    $(".menuitem").on("click", function(e) {
        e.preventDefault();
        //  $(document).off("scroll");
        var athis = this;
        var target = this.hash,
            menu = target;
        $target = $(target);
        $("html, body")
            .stop()
            .animate({
                    scrollTop: $target.offset().top + 2,
                },
                800,
                "swing",
                function() {
                    window.location.hash = target;
                    $(".menuitem").removeClass("active");
                    $(athis).addClass("active");
                }
            );
    });

    $(window).scroll(function(event) {
        var scrollPos = $(document).scrollTop();
        if (scrollPos === 0) {
            $('a[href^="#home"]').addClass("active");
            return;
        }

        $(".menuitem").each(function() {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                $(".menuitem").removeClass("active");
                currLink.addClass("active");
            } else {
                currLink.removeClass("active");
            }
        });
    });
});