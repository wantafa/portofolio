<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous" />
    {{-- Icon --}} 
    <link href="{{asset('depan/img/favicon.ico')}}" rel="icon">
    
    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <!-- My Css -->
    <link href='https://fonts.googleapis.com/css?family=Nunito' rel='stylesheet'>
    <link rel="stylesheet" href="{{ asset('depan/style.css') }}" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('depan/particles-depan.css') }}">
    
    <!-- magnific-popup css cdn link  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" />

    <title>Dzakwan | Portofolio</title>
</head>

<body id="home" class="bg-image">
    
    <!-- Navbar -->
    <nav id="main-navigation" class="navbar navbar-expand-lg navbar-dark shadow-sm fixed-top">
        <div class="container">
            <a class="navbar-brand" href="#">Dzakwan Al Fatah</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a id="s1" class="nav-link menuitem " aria-current="page" href="#home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a id="s2" class="nav-link menuitem " href="#about">About</a>
                    </li>
                    <!-- <li class="nav-item"> -->
                    <!-- <a id="s3" class="nav-link menuitem " href="#resume">Resume</a> -->
                    <!-- </li> -->
                    <li class="nav-item">
                        <a id="s4" class="nav-link menuitem " href="#projects">Projects</a>
                    </li>
                    <li class="nav-item">
                        <a id="s5" class="nav-link menuitem " href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Navbar Akhir -->
    <div id="particles-js" class="fixed"></div>

    <!-- Jumbotron -->
    <section class="jumbotron text-left">
        
        <!---- <img src="img/foto.jpg" alt="Dzakwan" width="200" class="rounded-circle img-thumbnail" /> -->
        <div class="h1_jumbotron text-light">My Name is Dzakwan Taqiyyuddin Al Fatah<br>I'm
            <div id="typed-strings">
                <p><strong>Web Developer</strong></p>
                <p><strong>Fullstack Developer</strong></p>
                <p><strong>Laravel Developer</strong></p>
                <p>Likes <em>to</em> learn.</p>
            </div>
            <span id="typed"></span>
        </div>
        <!-- <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"> -->
        <!-- <path -->
        <!-- fill="#ffffff" -->
        <!-- fill-opacity="1" -->
        <!-- d="M0,96L40,112C80,128,160,160,240,149.3C320,139,400,85,480,85.3C560,85,640,139,720,144C800,149,880,107,960,117.3C1040,128,1120,192,1200,192C1280,192,1360,128,1400,96L1440,64L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z" -->
        <!-- ></path> -->
        <!-- </svg> -->
    </section>
    <!-- Jumbotron Akhir -->

    <!-- About -->
    <section id="about">
        <div class="aboutme container">
            <div class="row judul mb-3" data-aos="zoom-in-left">
                <div class="col">
                    <h2>About Me</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 pl-5" data-aos="fade-right">
                    <img src="depan/img/zzz.jpg" class="img-fluid" alt="" height="20">
                </div>
                <div class="col-lg-8 pt-4 pt-lg-0 content text-white" data-aos="fade-left">
                    <h3>PROGRAMMER</h3>
                    <p class="fst-italic">
                        Dzakwan, lahir tahun 1997, berasal dari Tangerang. Sangat tertarik dalam dunia teknologi informasi, lebih tepatnya pada Software Development. Dengan keahlian yang saya miliki, saya yakin dapat berkontribusi pada pengembangan Software.
                    </p>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul>
                                <li><i class="bi bi-chevron-right"></i> <strong>Birthday:</strong> <span>18 Desember 1997</span></li>
                                <li><i class="bi bi-chevron-right"></i> <strong>Website:</strong> <span>dzakwan.dev</span></li>
                                <li><i class="bi bi-chevron-right"></i> <strong>Phone:</strong> <span>+62</span></li>
                                <li><i class="bi bi-chevron-right"></i> <strong>City:</strong> <span>Tangerang, Banten</span></li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul>
                                <li><i class="bi bi-chevron-right"></i> <strong>Age:</strong> <span>24</span></li>
                                <li><i class="bi bi-chevron-right"></i> <strong>Pendidikan:</strong> <span>Sarjana</span></li>
                                <li><i class="bi bi-chevron-right"></i> <strong>Email:</strong> <span>dzakwan@muslim.com</span></li>
                                <li><i class="bi bi-chevron-right"></i> <strong>Freelance:</strong> <span>Available</span></li>
                            </ul>
                        </div>
                    </div>
                    <p>
                        
                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- Akhir About -->

    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts section-bg">
        <div class="container" data-aos="fade-down">

            <div class="row justify-content-end">

                <div class="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
                    <div class="count-box">
                        <span data-purecounter-start="0" data-purecounter-end="14" data-purecounter-duration="2" class="purecounter"></span>
                        <p>Happy Clients</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
                    <div class="count-box">
                        <span data-purecounter-start="0" data-purecounter-end="14" data-purecounter-duration="2" class="purecounter"></span>
                        <p>Projects</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
                    <div class="count-box">
                        <span data-purecounter-start="0" data-purecounter-end="2" data-purecounter-duration="2" class="purecounter"></span>
                        <p>Years of experience</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
                    <div class="count-box">
                        <span data-purecounter-start="0" data-purecounter-end="1" data-purecounter-duration="2" class="purecounter"></span>
                        <p>Awards</p>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- End Counts Section -->

    <!-- Projects -->
    <section id="projects" class="de-gallery">
        <div class="row judul mb-3" data-aos="zoom-in-left">
            <div class="col">
                <h2>My Projects</h2>
            </div>
        </div>
        <div class="gallery" data-aos="zoom-in-left">
            <ul class="controls">
                <li class="buttons active" data-filter="all">all</li>
                @foreach ($kategori as $item)
                <li class="buttons" data-filter="{{ $item->kategori }}">{{ $item->kategori }}</li>
                @endforeach
            </ul>

            <div class="image-container">
                @foreach ($project as $item)
                <a href="{{ $item->foto }}" class="image {{ $item->kategori }} text-decoration-none">
                    <p class="text-light text-center fw-bold">{{ $item->nama }}</p>
                    <img src="{{ $item->foto }}" alt="" />
                </a>
                @endforeach
            </div>
        </div>

        <!-- <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"> -->
        <!-- <path -->
        <!-- fill="#ffffff" -->
        <!-- fill-opacity="1" -->
        <!-- d="M0,192L34.3,186.7C68.6,181,137,171,206,144C274.3,117,343,75,411,101.3C480,128,549,224,617,245.3C685.7,267,754,213,823,213.3C891.4,213,960,267,1029,256C1097.1,245,1166,171,1234,160C1302.9,149,1371,203,1406,229.3L1440,256L1440,320L1405.7,320C1371.4,320,1303,320,1234,320C1165.7,320,1097,320,1029,320C960,320,891,320,823,320C754.3,320,686,320,617,320C548.6,320,480,320,411,320C342.9,320,274,320,206,320C137.1,320,69,320,34,320L0,320Z" -->
        <!-- ></path> -->
        <!-- </svg> -->
    </section>
    <!-- Akhir Projects -->

<!-- About -->
<section>
    <div class="container">
        <div class="row judul skills" data-aos="zoom-in-left">
            <div class="col">
                <h2>TECHNOLOGIES I USE</h2>
            </div>
        </div>

        <div class="row">
            <div class=" content text-white" data-aos="fade-left">
                <div class="row">
                    <div class="skills">
                        <span>PHP</span>
                        <span>LARAVEL</span>
                        <span>HTML</span>
                        <span>CSS</span>
                        <span>JavaScript</span>
                        <span>NodeJS</span>
                        <span>JQuery</span>
                        <span>Git</span>
                        <span>Bootstrap</span>
                        <span>MySql</span>
                      </div>
                </div>
                <p>
                    
                </p>
            </div>
        </div>

    </div>
</section>
<!-- Akhir About -->

    <!-- Contact -->
    <section id="contact">
        <div class="container" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="1500">
            <div class="row judul">
                <div class="col">
                    <h2>Contact Me</h2>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <form action="{{ route('kontak.store') }}" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="nama" class="form-label text-light">Nama Lengkap</label>
                            <input type="text" name="nama" class="form-control" id="nama" aria-describedby="nama" />
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label text-light">Email</label>
                            <input type="email" name="email" class="form-control" id="email" aria-describedby="email" />
                        </div>
                        <div class="mb-3">
                            <label for="pesan" class="form-label text-light">Pesan</label>
                            <textarea type="text" name="pesan" class="form-control" id="pesan" aria-describedby="emailHelp" /></textarea>
                        </div>
                        <button type="submit" class="btn btn-secondary">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
        
    </section>
    <!-- Akhir Contact -->


    <section>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#00FFFF" fill-opacity="1" d="M0,160L26.7,154.7C53.3,149,107,139,160,149.3C213.3,160,267,192,320,186.7C373.3,181,427,139,480,128C533.3,117,587,139,640,165.3C693.3,192,747,224,800,224C853.3,224,907,192,960,186.7C1013.3,181,1067,203,1120,192C1173.3,181,1227,139,1280,128C1333.3,117,1387,139,1413,149.3L1440,160L1440,320L1413.3,320C1386.7,320,1333,320,1280,320C1226.7,320,1173,320,1120,320C1066.7,320,1013,320,960,320C906.7,320,853,320,800,320C746.7,320,693,320,640,320C586.7,320,533,320,480,320C426.7,320,373,320,320,320C266.7,320,213,320,160,320C106.7,320,53,320,27,320L0,320Z"></path>
            <div class="row kaki">
                <footer class="text-muted text-center" style="background-color: #00FFFF">
                    <p>Made with <i class="bi bi-suit-heart-fill text-danger"></i> by
                        <a href="/"class="text-muted fw-bold">Dzakwan</a>
                    </p>
                </footer>
            </div>
        </svg>
    </section>
    <!-- Footer -->
    
    @include('sweetalert::alert')

    <!-- jquery cdn link  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Akhir Footer -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
    <script src="{{ asset('depan/js/purecounter/purecounter.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.12"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="{{ asset('depan/js/depan.js') }}"></script>
    <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/js/page/modules-sweetalert.js') }}"></script>

    <!-- magnific popup js cdn link  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
</body>

</html>
<script>
    var typed = new Typed('#typed', {
        stringsElement: '#typed-strings',
        loop: true,
        typeSpeed: 60,
        backDelay: 300,
    });
</script>

<script>
    $(document).ready(function() {
        $(".buttons").click(function() {
            $(this).addClass("active").siblings().removeClass("active");

            var filter = $(this).attr("data-filter");

            if (filter == "all") {
                $(".image").show(400);
            } else {
                $(".image")
                    .not("." + filter)
                    .hide(200);
                $(".image")
                    .filter("." + filter)
                    .show(400);
            }
        });

        $(".gallery").magnificPopup({
            delegate: "a",
            type: "image",
            gallery: {
                enabled: true,
            },
            image: {
                titleSrc: 'Dzakwan'
            }
        });
    });
</script>

<script>
    AOS.init();
</script>