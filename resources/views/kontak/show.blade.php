<div class="row">
    <div class="card-body">
        <ul class="list-group">
          <li class="list-group-item">
            <h6 class="mb-0 mt-1">Nama Project</h6>
            <p class="mb-0">{{ $project->nama }}</p>
          </li>
          <li class="list-group-item">
            <h6 class="mb-0 mt-1">Deskripsi</h6>
            <p class="mb-0"><img width="300" height="250" class="center" src="{{$project->foto}}"/></p>

          </li>
        </ul>
      </div>