 <div class="row">
     <div class="col-md-6">
         <div class="form-group">
          <input type="hidden" value="{{ $project->id }}" id="id_data"/>
             <label>Nama Project</label>
             <input id="nama" name="nama" placeholder="Masukkan Nama Project" type="text" class="form-control"
                 value="{{$project->nama}}">
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Kategori Project</label>
             <input name="kategori" type="text" placeholder="Masukkan Kategori Project" class="form-control" value="{{$project->kategori}}">
         </div>
     </div>
     <div class="col-md-6">
         <div class="form-group">
             <label>Foto</label>
             <input name="foto1" type="hidden" style="margin-top: 20px;" class="form-control" value="{{$project->foto}}">
             <input name="foto" type="file" style="margin-top: 20px;" class="foto form-control border-primary">
         </div>
     </div>