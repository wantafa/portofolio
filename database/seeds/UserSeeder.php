<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([ 
            'name' => 'Dzakwan',
            'username' => 'dzakwan',
            'email' => 'admin@admin.com',
            'role' => 'admin',
            'password' => Hash::make('dzakwan1'),
        ]);
    }
}
