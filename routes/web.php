<?php

use App\Project;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes([
  'register' => false
]);

Route::get('/', function () {
    $project = Project::all();
    $kategori = Project::select('kategori')->groupBy('kategori')->get();

    return view('depan.depan', compact('project','kategori'));
});

Route::group(['middleware' => 'auth'], function() {

    // BARANG
    Route::get('/wantafa', 'HomeController@index')->name('dashboard');
    Route::get('/project/wantafa', 'CrudController@index')->name('project');
    Route::post('/project/wantafa', 'CrudController@store')->name('store');
    Route::get('/project/wantafa/{project}/edit', 'CrudController@edit')->name('edit');
    Route::patch('/project/wantafa/update/{project}', 'CrudController@update')->name('update');
    Route::get('/project/wantafa/show/{project}', 'CrudController@show')->name('show');
    Route::delete('/project/wantafa/{project}', 'CrudController@destroy')->name('project.delete');
    
    //KONTAK
    Route::get('/kontakwantafa', 'KontakController@index')->name('kontak');
    Route::post('/', 'KontakController@store')->name('kontak.store');
    Route::delete('/kontakwantafa/{kontak}', 'KontakController@destroy')->name('kontak.delete');

    // CRUD
    // Route::get('/kopit', 'CrudController@create')->name('wantafa');

  // Profile
    Route::get('profile/wantafa', 'Admin\UserController@profile')->name('profile');
    Route::post('profile/wantafa', 'Admin\UserController@update')->name('updateProfile');
    Route::post('profile/wantafa/image', 'Admin\UserController@ubahfoto')->name('ubahFoto');
    Route::patch('profile/wantafa/password', 'Admin\UserController@ubahPassword')->name('ubahPassword');

  // Manajemen User
  Route::get('/manajemen_user/wantafa', 'UserController@index')->name('manajemen_user.index');
  Route::post('/manajemen_user/wantafa', 'UserController@store')->name('manajemen_user.store');
  Route::get('/manajemen_user/wantafa/{manajemen_user}/edit', 'UserController@edit')->name('edit');
  Route::patch('/manajemen_user/wantafa/update/{manajemen_user}', 'UserController@update')->name('update');
  Route::get('/manajemen_user/wantafa/show/{manajemen_user}', 'UserController@show')->name('show');
  Route::delete('/manajemen_user/wantafa/{manajemen_user}', 'UserController@destroy')->name('manajemen_user.delete');
});



